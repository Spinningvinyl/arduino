#include <Adafruit_NeoPixel.h>
#define PIN 6
#define LED_COUNT 16
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_COUNT, PIN, NEO_GRBW + NEO_KHZ800);

void setup() {
  strip.setBrightness(20);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}
void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}

//This SETS THE PINS GLOBALY
//c programming
int redPin = 10;// int is an integer
int greenPin = 8;
int bluePin = 9;

void setup1() {
  Serial.begin(9600);//serial out
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  
}

//initialize n and count global
int n = 0;
int count;


void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}


void loop() {
  // Red to Green
  colorWipe(strip.Color(255,  0,  0), 50);
  setColor(255, 0, 0);
  colorWipe(strip.Color(255,  50,  0), 50);
  setColor(255, 50, 0);
  colorWipe(strip.Color(255,  100,  0), 50);
  setColor(255, 100, 0);
  colorWipe(strip.Color(255,  150,  0), 50);
  setColor(255, 150, 0);
  colorWipe(strip.Color(255,  200,  0), 50);
  setColor(255, 200, 0);
  colorWipe(strip.Color(255,  255,  0), 50);
  setColor(255, 255,  0);
  colorWipe(strip.Color(200,  255,  0), 50);
  setColor(200, 255,  0);
  colorWipe(strip.Color(150,  255,  0), 50);
  setColor(150, 255,  0);
  colorWipe(strip.Color(100,  255,  0), 50);
  setColor(100, 255,  0);
  colorWipe(strip.Color(50,  255,  0), 50);
  setColor(50, 255,  0);
  colorWipe(strip.Color(0,  255,  0), 50);
  setColor(0, 255,  0);
  // Green to Blue
  colorWipe(strip.Color(0,  255,  0), 50);
  setColor(0, 255,  50);
  colorWipe(strip.Color(0,  255,  50), 50);
  setColor(0, 255,  100);
  colorWipe(strip.Color(0,  255,  100), 50);
  setColor(0, 255,  150);
  colorWipe(strip.Color(0,  255,  150), 50);
  setColor(0, 255,  200);
  colorWipe(strip.Color(0,  255,  200), 50);
  setColor(0, 255,  255);
  colorWipe(strip.Color(0,  255,  255), 50);
  setColor(0, 200,  255);
  colorWipe(strip.Color(0,  200,  255), 50);
  setColor(0, 150,  255);
  colorWipe(strip.Color(0,  150,  255), 50);
  setColor(0, 100,  255);
  colorWipe(strip.Color(0,  100,  255), 50);
  setColor(0, 50,  255);
  colorWipe(strip.Color(0,  50,  255), 50);
  setColor(0, 0,  255);
  colorWipe(strip.Color(0,  0,  255), 50);
  // Blue to Red
  colorWipe(strip.Color(0,  0,  255), 50);
  setColor(0, 0,  255);
  colorWipe(strip.Color(50,  0,  255), 50);
  setColor(50, 0,  255);
  colorWipe(strip.Color(100,  0,  255), 50);
  setColor(100, 0,  255);
  colorWipe(strip.Color(150,  0,  255), 50);
  setColor(150, 0,  255);
  colorWipe(strip.Color(200,  0,  255), 50);
  setColor(200, 0,  255);
  colorWipe(strip.Color(255,  0,  255), 50);
  setColor(255, 0,  255);
  colorWipe(strip.Color(255,  0,  200), 50);
  setColor(255, 0,  200);
  colorWipe(strip.Color(255,  0,  150), 50);
  setColor(255, 0,  150);
  colorWipe(strip.Color(255,  0,  50), 50);
  setColor(255, 0,  100);
  colorWipe(strip.Color(255,  0,  0), 50);
  setColor(255, 0,  50);
  setColor(255, 0,  0);
  
}
