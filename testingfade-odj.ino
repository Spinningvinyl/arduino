#include <Adafruit_NeoPixel.h>
#define PIN 6
#define LED_COUNT 16
Adafruit_NeoPixel strip = Adafruit_NeoPixel(LED_COUNT, PIN, NEO_GRBW + NEO_KHZ800);


void setup() {
  strip.setBrightness(20);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}
void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}




void loop() {
  // Red to Green
  colorWipe(strip.Color(255,  0,  0), 50);
  colorWipe(strip.Color(255,  50,  0), 50);
  colorWipe(strip.Color(255,  100,  0), 50);
  colorWipe(strip.Color(255,  150,  0), 50);
  colorWipe(strip.Color(255,  200,  0), 50);
  colorWipe(strip.Color(255,  255,  0), 50);
  colorWipe(strip.Color(200,  255,  0), 50);
  colorWipe(strip.Color(150,  255,  0), 50);
  colorWipe(strip.Color(100,  255,  0), 50);
  colorWipe(strip.Color(50,  255,  0), 50);
  colorWipe(strip.Color(0,  255,  0), 50);
  // Green to Blue
  colorWipe(strip.Color(0,  255,  0), 50);
  colorWipe(strip.Color(0,  255,  50), 50);
  colorWipe(strip.Color(0,  255,  100), 50);
  colorWipe(strip.Color(0,  255,  150), 50);
  colorWipe(strip.Color(0,  255,  200), 50);
  colorWipe(strip.Color(0,  255,  255), 50);
  colorWipe(strip.Color(0,  200,  255), 50);
  colorWipe(strip.Color(0,  150,  255), 50);
  colorWipe(strip.Color(0,  100,  255), 50);
  colorWipe(strip.Color(0,  50,  255), 50);
  colorWipe(strip.Color(0,  0,  255), 50);
  // Blue to Red
  colorWipe(strip.Color(0,  0,  255), 50);
  colorWipe(strip.Color(50,  0,  255), 50);
  colorWipe(strip.Color(100,  0,  255), 50);
  colorWipe(strip.Color(150,  0,  255), 50);
  colorWipe(strip.Color(200,  0,  255), 50);
  colorWipe(strip.Color(255,  0,  255), 50);
  colorWipe(strip.Color(255,  0,  200), 50);
  colorWipe(strip.Color(255,  0,  150), 50);
  colorWipe(strip.Color(255,  0,  50), 50);
  colorWipe(strip.Color(255,  0,  0), 50);
  
}
