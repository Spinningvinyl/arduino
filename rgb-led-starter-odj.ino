//This SETS THE PINS GLOBALY
//c programming
int redPin = 4;// int is an integer
int greenPin = 3;
int bluePin = 2;

void setup() {
  Serial.begin(9600);//serial out
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  
}

//initialize n and count global
int n = 0;
int count;

void loop() {
  setColor(0,255 ,0);
  delay(250);
  setColor(255,220,0);
  delay(250); 
   setColor(255, n, 255); 
   n = n + 1;
   if(n > 255) n = 0;
   delay(250);
   Serial.print(n);
   Serial.print(" ");
   count++;
  if (count % 20 == 0)Serial.println();
  
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
